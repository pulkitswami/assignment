//
//  ViewModel.swift
//  Assign
//
//  Created by Bhagat Singh on 09/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import Foundation


class ViewModel : NSObject {
    
    class func callPhotoAPI(url:String,parent:AnyObject?,page : String,complition: @escaping (PhotoModel?, String) ->()) {
    let resource: Resource<PhotoModel>?
              resource = ResourceBuilder.photoApiResources(parent: parent, httpBody: nil,url:url,page: page)
              
                     Webservice.load(resource: resource) { result,errstr  in
                         switch result {
                         case .failure( _):
                             
                             complition(nil,errstr)
                             
                         case .success(let result):
                             complition(result,errstr)
                         }
                     }
           //  }
         }
    
}
