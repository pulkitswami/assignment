//
//  VC+CollectionViewDelegate.swift
//  Assign
//
//  Created by Bhagat Singh on 10/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import UIKit

extension ViewController : UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let imageView = UIImageView()
        
        if let data = photoDataModel?.photos.photo[indexPath.row]{
            let secret = data.secret
            let farm = String(data.farm)
            let server = data.server
            let id = data.id
            
            let url = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg"
            
            let imgURL = URL(string: url)
            
            imageView.kf.setImage(with: imgURL)
            imageView.frame = self.view.frame
            imageView.backgroundColor = .black
            imageView.contentMode = .center
            imageView.isUserInteractionEnabled = true
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissFullscreenImage))
            tap.numberOfTapsRequired = 2
            imageView.addGestureRecognizer(tap)
            
            let pinchMethod = UIPinchGestureRecognizer(target: self, action: #selector(pinchImage(sender:)))
            imageView.addGestureRecognizer(pinchMethod)
            
        }
        self.view.addSubview(imageView)
    }
    
    @objc func pinchImage(sender: UIPinchGestureRecognizer) {
        // guard let sender.view != nil else { return }
        
        if let scale = (sender.view?.transform.scaledBy(x: sender.scale, y: sender.scale)) {
            guard scale.a > 1.0 else { return }
            guard scale.d > 1.0 else { return }
            sender.view?.transform = scale
            sender.scale = 1.0
        }
    }
    
    
    // Use to back from full mode
    @objc func dismissFullscreenImage(_ sender: UITapGestureRecognizer) {
        sender.view?.removeFromSuperview()
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.size.width-50, height: 150)
        
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if (indexPath.row ==  (photoDataModel?.photos.photo.count)! - 1 ) { //it's your last cell
            //Load more data & reload your collection view
            page = page + 1
            ViewModel.callPhotoAPI(url: "", parent: self, page: String(page)) { (data, error) in
                if(data != nil){
                    if let arr = data?.photos.photo{
                        
                        for obj in arr{
                            self.photoDataModel?.photos.photo.append(obj)
                        }
                        
                        
                    }            }}
        }
    }
    
}
