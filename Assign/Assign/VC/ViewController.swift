//
//  ViewController.swift
//  Assign
//
//  Created by Bhagat Singh on 09/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import UIKit
import Kingfisher

class ViewController: UIViewController {

    var page = 1
    
    @IBOutlet weak var photoCollectionView: UICollectionView!
    
    @IBOutlet weak var toggleBtn: UIButton!
    var photoDataModel: PhotoModel?{
        didSet{
            
            DispatchQueue.main.async {
                self.photoCollectionView.reloadData()
            
            }
        }
    }
    
     
    override func viewDidLoad() {
        super.viewDidLoad()
        photoCollectionView.delegate = self
        photoCollectionView.dataSource = self
    
       
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        ViewModel.callPhotoAPI(url: "", parent: self, page: String(page)) { (data, error) in
            if(data != nil){
                
                self.photoDataModel = data
            }
        }
        
    }
        

    @IBAction func toggleButton(_ sender: Any) {
        
        if(toggleBtn.titleLabel?.text == "Grid"){
            toggleBtn.setTitle("List", for: .normal)
            
            if let layout = photoCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .horizontal
            }
            
            
            
        }else{
            toggleBtn.setTitle("Grid", for: .normal)
            if let layout = photoCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
                layout.scrollDirection = .vertical
            }        }
        
    }
}

