//
//  VC+UICollectionView.swift
//  Assign
//
//  Created by Bhagat Singh on 10/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import UIKit

extension ViewController : UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count  = (photoDataModel?.photos.photo.count){
            return count        }
       
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCollectionViewCell", for: indexPath) as! PhotoCollectionViewCell
        
        guard let data = photoDataModel?.photos.photo[indexPath.row] else {
            
            return UICollectionViewCell() }
        cell.layoutIfNeeded()
        cell.populateData(data: data)
        
        return cell
    }
    
    
    
    
}
