//
//  Helper.swift
//  Assign
//
//  Created by Bhagat Singh on 10/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import Foundation

class Helper : NSObject {
    
    


    class func isInternetConnectionAvailable()->Bool{
        let reachability: Reachability? = Reachability.networkReachabilityForInternetConnection()
        
        
        let status = reachability?.currentReachabilityStatus
        
        if(status == ReachabilityStatus.notReachable){
            
            SharedClass.sharedInstance.alertWindow(title: "No Internet", message: "Could not connect to internet. Please Check your internet connection")
            
        }
        
        return true
    }
     
    
 
    class func showNetworkAlert(){
        
        
    }}
