//
//  PhotoCollectionViewCell.swift
//  Assign
//
//  Created by Bhagat Singh on 09/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//

import UIKit

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var FotoImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    
    
    func populateData(data: Photo) {
        
        let secret = data.secret
        let farm = String(data.farm)
        let server = data.server
        let id = data.id
        
        let url = "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret).jpg"
        
        let imgURL = URL(string: url)
        self.FotoImageView.kf.setImage(with: imgURL)
        
    }
    
    
}
