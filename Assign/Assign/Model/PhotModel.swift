//
//  PhotModel.swift
//  Assign
//
//  Created by Bhagat Singh on 09/09/20.
//  Copyright © 2020 Bhagat Singh. All rights reserved.
//


//   let welcome = try? newJSONDecoder().decode(Welcome.self, from: jsonData)

import Foundation

// MARK: - Welcome
struct PhotoModel: Codable {
    var photos: Photos
    let stat: String
}

// MARK: - Photos
struct Photos: Codable {
    let page, pages, perpage: Int
    let total: String
    var photo: [Photo]
}

// MARK: - Photo
struct Photo: Codable {
    let id, owner, secret, server: String
    let farm: Int
    let title: String
    let ispublic, isfriend, isfamily: Int
}
