//
//  Resource.swift
//  
//



import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case put = "PUT"
    case post = "POST"
    case delete = "DELETE"
}

struct Resource<A> {
    let url: String
    let api:String?
    let parent: AnyObject?
    let httpBody: Data?
    let method: HTTPMethod
    let parse: (Data) -> A?
}

struct LocalResources<L> {
    let parent: AnyObject?
    let api:String?
     let parse: (Data) -> L?
}
