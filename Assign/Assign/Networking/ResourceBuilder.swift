//
//  URLBuilder.swift


import Foundation

struct ResourceBuilder {
    
    
    
    static func photoApiResources(parent:AnyObject?,httpBody:Data?,url:String,page:String) -> Resource<PhotoModel>? {
            
            let strUrl = "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=3e7cc266ae2b0e0d78e279ce8e361736&format=json&nojsoncallback=1&safe_search=1&tags=kitten&per_page=10&page="+page
        
             return Resource(url: strUrl, api: "", parent: parent, httpBody: httpBody, method: .get) { (dataValue) in
                    do {
                        let modal = try JSONDecoder()
                            .decode(PhotoModel.self, from: dataValue)
                        return modal
                    } catch {
                        print(error)
                        return nil
                    }
                }
           
    }
    
}
   


