//
//  Webservice.swift
//
//
import UIKit
import SystemConfiguration

enum NetworkError: Error {
    case parseUrl
    case parseJson
    case parseData
    case emptyResource
}

enum Webservice {
    
    static var numberOfRequests = 0 {
        didSet {
            DispatchQueue.main.async {
                //UIApplication.shared.isNetworkActivityIndicatorVisible = numberOfRequests > 0
            }
        }
    }
    
    
    

    
    @discardableResult
    static func load<A>(resource: Resource<A>?, completion: @escaping (Result<A, Error>,String) -> Void) -> URLSessionTask? {
        var errorStr = "Something went wrong, please try later."
        guard let resource = resource else {
            completion(.failure(NetworkError.emptyResource), errorStr)
            return nil
        }
        
        guard let url = URL(string: resource.url) else {
            completion(.failure(NetworkError.parseUrl), errorStr)
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = resource.method.rawValue
        
      
        
        if !Helper.isInternetConnectionAvailable() {
                Helper.showNetworkAlert()
                
             
                
        }
        
    
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 50.0
        
        let session = URLSession(configuration: configuration)
        
      
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
    
        numberOfRequests += 1
        let task = session.dataTask(with: request) { data, response, error in
            
            numberOfRequests -= 1
            
            
            
            var decryptedResponseData = data
            if(data != nil){
                let strEncryptedResponse = String(data: decryptedResponseData!, encoding: .utf8)
                
                var timestampString : String = ""
                if #available(iOS 13.0, *) {
                    let timestamp = NSDate().timeIntervalSince1970 * 1000
                    print(timestamp)
                    timestampString = String(timestamp)
                    print(timestampString)
                } else {
                }
                
                decryptedResponseData = strEncryptedResponse?.data(using: .utf8)
                
                let httpResponse = response as? HTTPURLResponse
                print(request.url ?? "")
                print(httpResponse?.statusCode ?? 00)
                
                if errorStr.contains("DOCTYPE"){
                    errorStr = "Something went wrong, please try later."
                }
            }
            
            guard error == nil, let _ = decryptedResponseData else {
                completion(.failure(error!), errorStr)
                return
            }
            guard let result = resource.parse(decryptedResponseData!) else {
                completion(.failure(NetworkError.parseData), errorStr)
                return
            }
            completion(.success(result), errorStr)
        }
        
        task.resume()
        return task
    }
    
    
  
    
}
